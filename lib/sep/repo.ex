defmodule Sep.Repo do
  use Ecto.Repo,
    otp_app: :sep,
    adapter: Ecto.Adapters.Postgres
end
