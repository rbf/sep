defmodule SepWeb.PageController do
  use SepWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
